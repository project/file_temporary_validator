<?php

namespace Drupal\file_temporary_validator\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;

/**
 * Class File Temporary Validator.
 *
 *  @package file_temporary_validator
 */
class FileTemporaryValidator {
  use StringTranslationTrait;

  /**
   * String translation service.
   *
   * @var \Drupal\Core\StringTranslation\TranslationInterface
   */
  protected $translator;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs the file temporary validator service.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translator
   *   The string translation service object.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(TranslationInterface $translator,
    EntityTypeManagerInterface $entityTypeManager,
    AccountInterface $currentUser
    ) {
    $this->translator = $translator;
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
  }

  /**
   * Check Temporary Files function.
   *
   * @param \Drupal\file\Entity\File $file
   *   File.
   *
   * @return array
   *   Return Response Array.
   */
  public function checkTemporaryFileViolation(File $file): array {
    if ($found_file = $this->getFileDuplications($file)) {
      // If we have access to the files overview, link to the file.
      if ($this->currentUser->hasPermission('access files overview')) {
        try {
          $url = Url::fromRoute('view.files.page_1', ['filename' => $found_file->getFilename()], ['attributes' => ['target' => '_blank']]);
          return [
            new TranslatableMarkup("Error while uploading file: Temporary file '@file_name' already exists at '@file_location'.", [
              '@file_name' => Link::fromTextAndUrl($this->entityTypeManager->getStorage('file')->load($found_file->id())->label(), $url)->toString(),
              '@file_location' => $file->getFileUri(),
            ]),
          ];
        }
        catch (\Exception $e) {
          // View must be disabled.
        }
      }

      return [
        new TranslatableMarkup("Error while uploading file: Temporary file '@file_name' already exists at '@file_location'.", [
          '@file_name' => $file->getFilename(),
          '@file_location' => $file->getFileUri(),
        ]),
      ];
    }
    return [];
  }

  /**
   * Queries and retrieves any temporary file duplications.
   *
   * @param \Drupal\file\Entity\File $file
   *   File.
   *
   * @return \Drupal\file\FileInterface|null
   *   Return the file if found.
   */
  public function getFileDuplications(File $file): FileInterface|null {
    $fileStorage = $this->entityTypeManager->getStorage('file');
    $fids = $fileStorage->getQuery()
      ->accessCheck(TRUE)
      ->condition('status', FileInterface::STATUS_PERMANENT, '<>')
      ->condition('filename', $file->getFilename())
      ->range(0, 1)
      ->execute();

    if ($fid = reset($fids)) {
      return $fileStorage->load($fid);
    }
    return NULL;
  }

}
