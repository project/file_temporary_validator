CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides content editors validation to prevent uploading duplicate
file names into the temporary directory. It will alert the user if a file
already exists, and provides a link to delete if they have permissions.

This is useful for preventing content editors from having files with numbers
appended to them (e.g. filename_1). This is a common occurrence when working
with multiple tabs open with partially finished content edits.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/file_temporary_validator

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/file_temporary_validator


REQUIREMENTS
------------

 * Drupal 9 or 10.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

 * Browse to '/admin/structure/media' or '/admin/structure/types' location.

 * From operations section, select 'Manage fields'.

 * Select any file type field and click edit.

 * Enable "Enable File Temporary Validator".


MAINTAINERS
-----------

Current maintainers:
 * Jordan Barnes (j-barnes) - https://www.drupal.org/u/j-barnes
